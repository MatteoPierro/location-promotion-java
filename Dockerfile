FROM openjdk:11

WORKDIR /usr/src/app

COPY target/location-promotion-service-fat.jar location-promotion-service.jar

CMD ["java", "-jar", "location-promotion-service.jar"]