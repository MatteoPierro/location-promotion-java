# Location Promotion Service

## Test

### 1- setup test

```java
@ExtendWith(PactConsumerTestExt.class)
@ExtendWith(MockitoExtension.class)
@PactTestFor(providerName = "Sign up Service")
public class PromotionControllerTest {

    @BeforeEach
    public void setUp(MockServer signUpService) throws Exception {
    }
}
```

### 2- instantiate the system under test

```java
    private SpyMailService mailService;
    private PromotionController promotionController;

    @BeforeEach
    public void setUp(MockServer signUpService) throws Exception {
        mailService = new SpyMailService();
        promotionController = new PromotionController(signUpService.getUrl(), mailService);
    }

    private class SpyMailService implements MailService{
        private List<Email> mailsSent = new ArrayList<>();

        @Override
        public void send(Email email) {
            mailsSent.add(email);
        }
    }
```

### 3- Add Test

```java
    @Mock Request request;
    @Mock Response response;
    
    @Test
    @PactTestFor(pactMethod = "hasUsersInBelgium")
    public void shouldSendEmailsForAllUsers() {
        when(request.queryParams("location")).thenReturn("Belgium");

        promotionController.sendPromotion(request, response);

        verify(response).status(202);
        assertEquals(2, mailService.mailsSent.size());
        assertEquals("foo@bar.com", mailService.mailsSent.get(0).receiver());
        assertEquals("fizz@baz.be", mailService.mailsSent.get(1).receiver());
    }
```

### 4- Add interaction with provider

```java

    @Pact(provider = "Sign up Service", consumer = "Location Promotion Service", state = "has users in Belgium")
    public RequestResponsePact hasUsersInBelgium(PactDslWithProvider builder) {
        return builder
                .given("has users in Belgium")
                    .uponReceiving("a request for Users")
                    .path("/api/users")
                    .query("location=Belgium")
                    .method("GET")
                    .willRespondWith()
                .status(200)
                    .headers(Map.of("Content-Type", "application/json"))
                    .body(usersInBelgium())
                .toPact();
    }

    private String usersInBelgium() {
        JsonObject matteo = new JsonObject()
                .add("first_name", "Matteo")
                .add("last_name", "Pierro")
                .add("email", "foo@bar.com");
        JsonObject nelis  = new JsonObject()
                .add("first_name", "Nelis")
                .add("last_name", "Boucke")
                .add("email", "fizz@baz.be");
        return new JsonArray()
                .add(matteo)
                .add(nelis)
                .toString();
    }
```

### 5- Controller Implementation

#### A.

```java
    public String sendPromotion(Request request, Response response) {
        try {
            String location = request.queryParams("location");
            JsonArray users = usersFor(location);
            sendEmailTo(users, location);
            response.status(202);
        } catch (Exception ex) {
            response.status(500);
        }

        return "";
    }
```

#### B.
```java
    private JsonArray usersFor(String location) throws IOException, InterruptedException {
        HttpClient httpClient = HttpClient.newBuilder().build();
        HttpResponse<String> httpResponse = httpClient.send(requestForUsersIn(location), ofString());
        return Json.parse(httpResponse.body()).asArray();
    }

    private HttpRequest requestForUsersIn(String location) {
        return HttpRequest.newBuilder()
                    .uri(create(signUpURL + "/api/users?location=" + location))
                    .GET()
                    .build();
    }
```

#### C.
```java
    private void sendEmailTo(JsonArray users, String location) {
        StreamSupport.stream(users.spliterator(), false)
                .map(JsonValue::asObject)
                .map(user -> emailFor(user, location))
                .forEach(mailService::send);
    }

    private Email emailFor(JsonObject members, String location) {
        String firstName = members.getString("first_name", "");
        String lastName = members.getString("last_name", "");
        String email = members.getString("email", "");
        String subject = "Great Promotion in " + location;
        String text = "Hi " + firstName + lastName + "!\n"
                + "We have a great promotion in " + location;

        return new Email(email, subject, text);
    }
```

### Second Test

#### A.

```java
    @Test
    @PactTestFor(pactMethod ="hasNoUsersInFrance")
    public void shouldNotSendEmailsWhenThereAreNoUsers() {
        when(request.queryParams("location")).thenReturn("France");

        promotionController.sendPromotion(request, response);

        verify(response).status(202);
        assertTrue(mailService.sentMails.isEmpty());
    }
```

#### B.

```java
    @Pact(provider = "Sign up Service", consumer = "Location Promotion Service", state = "has no users in France")
    public RequestResponsePact hasNoUsersInFrance(PactDslWithProvider builder) {
        return builder
                .given("has no users in France")
                    .uponReceiving("a request for Users")
                    .path("/api/users")
                    .query("location=France")
                    .method("GET")
                .willRespondWith()
                    .status(200)
                    .headers(Map.of("Content-Type", "application/json"))
                    .body(noUsers())
                .toPact();
    }

    private String noUsers() {
        return new JsonArray().toString();
    }
```