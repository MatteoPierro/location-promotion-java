package com.wesquad.promotion.api;

import com.wesquad.promotion.email.MailService;
import spark.Request;
import spark.Response;

public class PromotionController {

    private final String signUpURL;
    private final MailService mailService;

    public PromotionController(String signUpURL, MailService mailService) {
        this.signUpURL = signUpURL;
        this.mailService = mailService;
    }

    public String sendPromotion(Request request, Response response) {
        return "";
    }
}
