package com.wesquad.promotion.api;

import static spark.Spark.post;

public class Router {

    private final PromotionController promotionController;

    public Router(PromotionController promotionController) {
        this.promotionController = promotionController;
    }

    public void create() {
        post("/sendPromotions", promotionController::sendPromotion);
    }
}
