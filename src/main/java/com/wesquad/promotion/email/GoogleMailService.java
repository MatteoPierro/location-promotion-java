package com.wesquad.promotion.email;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Base64;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.Message;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Properties;

import static java.util.Collections.singletonList;
import static javax.mail.Message.RecipientType.TO;

public class GoogleMailService implements MailService {
    private static final String APPLICATION_NAME = "workshop-consumer-driven";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "./secret";
    private static final List<String> SCOPES = singletonList(GmailScopes.GMAIL_SEND);
    private static final String CREDENTIALS_FILE_PATH = "./secret/gmail.credentials.json";

    @Override
    public void send(Email email) {
        try {
            Message message = messageFor(email);
            gmail().users().messages().send("me", message).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Message messageFor(Email email) throws MessagingException, IOException {
        Message message = new Message();

        MimeMessage emailMessage = mimeMessageFor(email);
        String encodedEmail = encode(emailMessage);
        message.setRaw(encodedEmail);

        return message;
    }

    private String encode(MimeMessage emailMessage) throws IOException, MessagingException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        emailMessage.writeTo(buffer);
        return Base64.encodeBase64URLSafeString(buffer.toByteArray());
    }

    private MimeMessage mimeMessageFor(Email email) throws MessagingException {
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        MimeMessage emailMessage = new MimeMessage(session);
        emailMessage.addRecipient(TO, new InternetAddress(email.receiver()));
        emailMessage.setSubject(email.subject());
        emailMessage.setText(email.text());
        return emailMessage;
    }

    private Gmail gmail() throws GeneralSecurityException, IOException {
        NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        return new Gmail.Builder(httpTransport, JSON_FACTORY, getCredentials(httpTransport))
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    private static Credential getCredentials(final NetHttpTransport httpTransport) throws IOException {
        GoogleAuthorizationCodeFlow flow = authorizationFlow(httpTransport);
        return new AuthorizationCodeInstalledApp(flow, receiver()).authorize("user");
    }

    private static GoogleAuthorizationCodeFlow authorizationFlow(NetHttpTransport HTTP_TRANSPORT) throws IOException {
        return new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecret(), SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
    }

    private static GoogleClientSecrets clientSecret() throws IOException {
        InputStream in = new FileInputStream(new File(CREDENTIALS_FILE_PATH));
        return GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
    }

    private static LocalServerReceiver receiver() {
        return new LocalServerReceiver.Builder().setPort(8888).build();
    }
}