package com.wesquad.promotion.email;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;

public class Email {
    private final String receiver;
    private final String subject;
    private final String text;

    public Email(String receiver, String subject, String text) {
        this.receiver = receiver;
        this.subject = subject;
        this.text = text;
    }

    public String receiver() {
        return receiver;
    }

    public String subject() {
        return subject;
    }

    public String text() {
        return text;
    }

    @Override
    public boolean equals(Object o) {
        return reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return reflectionHashCode(this);
    }
}
