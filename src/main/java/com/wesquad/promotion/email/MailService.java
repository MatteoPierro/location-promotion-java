package com.wesquad.promotion.email;

public interface MailService {

    void send(Email email);
}
