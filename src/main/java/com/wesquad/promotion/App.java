package com.wesquad.promotion;

import static spark.Spark.before;
import static spark.Spark.port;

import com.wesquad.promotion.api.PromotionController;
import com.wesquad.promotion.api.Router;
import com.wesquad.promotion.email.GoogleMailService;
import com.wesquad.promotion.email.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

    private static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        port(listeningPort());
        setLog();
        router().create();
    }

    private static int listeningPort() {
        String port = System.getenv("LOCATION_PROMOTION_PORT");
        return port != null
                ? Integer.parseInt(port)
                : 2222;
    }

    private static void setLog() {
        before((request, response) -> {
            logger.info("URL request: " + request.requestMethod() + " " + request.uri() + " - headers: " + request.headers());
        });
    }

    private static Router router() {
        MailService mailService = new GoogleMailService();
        PromotionController promotionController = new PromotionController(signUpURL(), mailService);
        return new Router(promotionController);
    }

    private static String signUpURL() {
        String singUpURL = System.getenv("SIGN_UP_SERVICE_URL");
        return singUpURL != null
                ? singUpURL
                : "http://localhost:7777";
    }
}
